FROM ubuntu:latest

LABEL mantainer="fabifont"

RUN ln -snf /usr/share/zoneinfo/Europe/Rome /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get update \
     && apt-get install -qy texlive-full \
     && apt-get install -qy python3 python3-pygments curl git \
